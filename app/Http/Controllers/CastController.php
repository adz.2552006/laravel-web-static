<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class CastController extends Controller
{
    public function create(){
        return view('crudcast.create');
    }
    public function store(Request $request){
        $request->validate([
            'name' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

         DB::table('cast')->insert([
        'name' => $request['name'],
        'umur' => $request['umur'],
        'bio' => $request['bio']
         ]);   
         return redirect('/cast'); 
    }  

    public function index(){
        $cast = DB::table('cast')->get();

        return view('crudcast.index', compact('cast'));
    }

    public function show($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('crudcast.show', compact('cast'));
    }

    public function edit($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('crudcast.edit', compact('cast'));
    }

    public function update($id, Request $request){
        $request->validate([
            'name' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        $query = DB::table('cast')
              ->where('id', $id)
              ->update([
                  'name' => $request['name'],
                  'umur' => $request['umur'],
                  'bio' => $request['bio']
            ]);
            return redirect('/cast');
    }
    public function destroy($id){
        DB::table('cast')->where('id', $id)->delete();
        
        return redirect('/cast');
    }
}
